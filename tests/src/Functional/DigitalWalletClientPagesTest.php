<?php

namespace Drupal\Tests\digital_wallet_client\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Functional tests for the digital_wallet_client module.
 *
 * @group digital_wallet_client
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class DigitalWalletClientPagesTest extends BrowserTestBase {

  /**
   * The admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'jquery_colorpicker',
    'digital_wallet_client',
  ];

  /**
   * Permissions to grant admin user.
   *
   * @var array
   */
  protected $permissions = [
    'access administration pages',
    'administer digital wallet client',
  ];

  /**
   * Sets the test up.
   */
  protected function setUp() {
    parent::setUp();
    // Test admin user.
    $this->adminUser = $this->drupalCreateUser($this->permissions);
  }

  /**
   * Test Android Classes Config Page.
   */
  public function testAndroidClassesConfigPage() {
    $assert_session = $this->assertSession();
    $this->drupalLogin($this->adminUser);

    $config_path = '/admin/config/digital-wallet/android-classes-types';
    // Fetch the Apple Wallet settings form.
    $this->drupalGet($config_path);

    $assert_session->statusCodeEquals(200);
  }

  /**
   * Test Manage Android Classes Config Page.
   */
  public function testManageAndroidClassesConfigPage() {
    $assert_session = $this->assertSession();
    $this->drupalLogin($this->adminUser);

    $config_path = '/admin/config/digital-wallet/android-classes';
    // Fetch the Apple Wallet settings form.
    $this->drupalGet($config_path);

    $assert_session->statusCodeEquals(200);
  }

  /**
   * Test Manage Cards Config Page.
   */
  public function testLayoutTypesConfigPage() {
    $assert_session = $this->assertSession();
    $this->drupalLogin($this->adminUser);

    $config_path = '/admin/config/digital-wallet/layout-types';
    // Fetch the Apple Wallet settings form.
    $this->drupalGet($config_path);

    $assert_session->statusCodeEquals(200);
  }

  /**
   * Test Android Classes Config Page.
   */
  public function testManageCardsConfigPage() {
    $assert_session = $this->assertSession();
    $this->drupalLogin($this->adminUser);

    $config_path = '/admin/config/digital-wallet/layouts';
    // Fetch the Apple Wallet settings form.
    $this->drupalGet($config_path);

    $assert_session->statusCodeEquals(200);
  }

}
