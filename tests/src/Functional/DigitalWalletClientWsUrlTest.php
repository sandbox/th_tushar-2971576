<?php

namespace Drupal\Tests\digital_wallet_client\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Functional tests for the digital_wallet_client module.
 *
 * @group digital_wallet_client
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class DigitalWalletClientWsUrlTest extends BrowserTestBase {

  /**
   * The admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'jquery_colorpicker',
    'digital_wallet_client',
  ];

  /**
   * Permissions to grant admin user.
   *
   * @var array
   */
  protected $permissions = [
    'access administration pages',
    'administer digital wallet client',
  ];

  /**
   * Sets the test up.
   */
  protected function setUp() {
    parent::setUp();
    // Test admin user.
    $this->adminUser = $this->drupalCreateUser($this->permissions);
  }

  /**
   * Test WS base URL setting.
   */
  public function testWsBaseUrl() {
    $assert_session = $this->assertSession();
    $this->drupalLogin($this->adminUser);

    $config_path = '/admin/config/digital-wallet/settings';
    // Fetch the Main Wallet API settings form.
    $this->drupalGet($config_path);

    $assert_session->statusCodeEquals(200);
  }

}
