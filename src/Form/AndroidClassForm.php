<?php

namespace Drupal\digital_wallet_client\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\digital_wallet_client\AndroidClassManager;
use Drupal\digital_wallet_client\StatusCodes;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * Form handler for an Android Class add and edit forms.
 */
class AndroidClassForm extends EntityForm {

  /**
   * Android Class Manager.
   *
   * @var \Drupal\digital_wallet_client\AndroidClassManager
   */
  protected $androidClassManager;

  /**
   * Serializer component.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * Constructs an Wallet Layout Config Form object.
   *
   * @param \Drupal\Core\Entity\Query\QueryFactory $entity_query
   *   The entity query.
   * @param \Drupal\digital_wallet_client\AndroidClassManager $android_class_manager
   *   Android Class manager.
   * @param \Symfony\Component\Serializer\Serializer $serializer
   *   Serializer component.
   */
  public function __construct(QueryFactory $entity_query, AndroidClassManager $android_class_manager, Serializer $serializer) {
    $this->entityQuery = $entity_query;
    $this->androidClassManager = $android_class_manager;
    $this->serializer = $serializer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.query'),
      $container->get('digital_wallet_client.manager.android_class'),
      $container->get('serializer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $walletlayout = $this->entity;

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $walletlayout->title,
      '#description' => $this->t("The administrative title of the wallet layout."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $walletlayout->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$walletlayout->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => t('Enabled?'),
      '#default_value' => isset($walletlayout->status) ? $walletlayout->status : '',
      '#description' => t('Whether the layout is enabled / disabled.'),
      '#weight' => 9,
    ];

    if (!$this->exist($walletlayout->id) || empty($walletlayout->type)) {
      $plugin_definitions = $this->androidClassManager->getDefinitions();
      $walletlayout_options = [];
      foreach ($plugin_definitions as $id => $plugin) {
        $walletlayout_options[$id] = $plugin['admintitle']->render();
      }

      $current_user = \Drupal::currentUser();
      $form['type'] = [
        '#type' => 'select',
        '#title' => t('Class Type'),
        '#options' => $walletlayout_options,
        '#default_value' => isset($walletlayout->type) ? $walletlayout->type : '',
        '#description' => t('Type of Wallet Layout.'),
        '#weight' => 8,
      ];

      $form['platform'] = [
        '#type' => 'hidden',
        '#value' => $plugin['platform']->render(),
      ];

      $form['uid'] = [
        '#type' => 'hidden',
        '#value' => $current_user->id(),
      ];
    }
    else {
      $layout_instance = $this->androidClassManager->createInstance($walletlayout->type);
      $form += $layout_instance->configForm($walletlayout);
    }

    if (isset($walletlayout->type)) {
      $form['title']['#prefix'] = '<div class="color-pallete-parent"><div class="color-pallete">';

      $default_wallet_img = drupal_get_path('module', 'digital_wallet_client') . '/images/loyalty.png';

      $form['wallet_image'] = [
        '#markup' => '</div><div class="loyalty-img"><img src="/' . $default_wallet_img . '" /></div>',
        '#weight' => 9999,
      ];

      $form['#attached']['library'] = [
        'digital_wallet_client/wallet_loyalty',
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $androidclass = $this->entity;
    $values = $form_state->getValues();

    $layout_instance = $this->androidClassManager->createInstance($androidclass->type);
    if ($this->exist($androidclass->id) || empty($androidclass->type)) {
      $id = $androidclass->id();

      $config = \Drupal::config('digital_wallet_client.main_settings');
      $encoder = \Drupal::getContainer()->get('serializer.encoder.json');
      $androidclass_data = $encoder->decode($androidclass->data, 'json');
      $ws_url = $config->get('webservice_base_url');

      if (is_numeric($values['digital_wallet_client_loyaltyclass_logo']['0']) && $values['digital_wallet_client_loyaltyclass_logo']['0']) {
        $loyaltyclass_logo_file = File::load($values['digital_wallet_client_loyaltyclass_logo']['0']);
        if ($loyaltyclass_logo_file->isTemporary()) {
          $loyaltyclass_logo_file->setPermanent();
          $loyaltyclass_logo_file->save();
        }

        $logo_url = file_create_url($loyaltyclass_logo_file->getFileUri());
        $logo_url_http = str_replace('https://', 'http://', $logo_url);
      }
      else {
        $logo_url_http = str_replace('https://', 'http://', $values['digital_wallet_client_loyaltyclass_logo']);
      }

      if (!empty($values['digital_wallet_client_loyaltyclass_heroimage'])) {
        if (is_numeric($values['digital_wallet_client_loyaltyclass_heroimage']['0']) && $values['digital_wallet_client_loyaltyclass_heroimage']['0']) {
          $loyaltyclass_hero_file = File::load($values['digital_wallet_client_loyaltyclass_heroimage']['0']);
          if ($loyaltyclass_hero_file->isTemporary()) {
            $loyaltyclass_hero_file->setPermanent();
            $loyaltyclass_hero_file->save();
          }

          $hero_image_url = file_create_url($loyaltyclass_hero_file->getFileUri());
          $hero_image_url_http = str_replace('https://', 'http://', $hero_image_url);
          $request['data']['hero_image'] = $hero_image_url_http;
        }
        else {
          $hero_image_url_http = str_replace('https://', 'http://', $values['digital_wallet_client_loyaltyclass_heroimage']);
          $request['data']['hero_image'] = $hero_image_url_http;
        }
      }

      $request = $layout_instance->defaultData();
      $request['data']['id'] = $values['digital_wallet_client_loyaltyclass_class_id'];
      $request['data']['program_name'] = $values['digital_wallet_client_loyaltyclass_progname'];
      $request['data']['issuer_name'] = $values['digital_wallet_client_loyaltyclass_issuername'];
      $request['data']['program_logo'] = $logo_url_http;
      $request['data']['reward_tier_label'] = $values['digital_wallet_client_loyaltyclass_rewardlabel'];
      $request['data']['reward_tier_value'] = $values['digital_wallet_client_loyaltyclass_rewardvalue'];
      $request['data']['account_name_label'] = $values['digital_wallet_client_loyaltyclass_accountnamelabel'];
      $request['data']['account_id_label'] = $values['digital_wallet_client_loyaltyclass_accountidlabel'];
      $request['data']['backgroundcolor'] = '#' . $values['digital_wallet_client_loyaltyclass_backcolor'];

      $client = \Drupal::httpClient();

      $oauth_details = digital_wallet_client_oauth_authenticate();

      $auth_token = $oauth_details['token_type'] . ' ' . $oauth_details['access_token'];

      $headers = [
        'Content-Type' => 'application/x-www-form-urlencoded',
        'Authorization' => $auth_token,
      ];

      if (empty($androidclass_data['data']['id'])) {
        try {
          $response_json = $client->post($ws_url . '/android-class-insert', [
            'headers' => $headers,
            'form_params' => ['data' => $encoder->encode($request, 'json')],
          ])->getBody()->getContents();

          $response = (array) \GuzzleHttp\json_decode($response_json);

          if ($response['status'] == StatusCodes::HTTP_OK) {
            drupal_set_message(t('Android class "@title" created on Google successfully.', ['@title' => $values['digital_wallet_client_loyaltyclass_title']]));
          }
          elseif ($response['status'] == StatusCodes::HTTP_BAD_REQUEST) {
            if ($response['reason'] == 'existingResource') {
              $form_state->setErrorByName('digital_wallet_client_loyaltyclass_class_id', t('Class ID "@class" exists on Google server.', ['@class' => $values['digital_wallet_client_loyaltyclass_class_id']]));
            }
          }
          else {
            $form_state->setErrorByName('digital_wallet_client_loyaltyclass_class_id', t('Android class "@class" couldn\'t be updated on Google. Please try again.', ['@class' => $values['digital_wallet_client_loyaltyclass_class_id']]));
          }
        }
        catch (ClientException $e) {
          if ($e->getCode() == StatusCodes::HTTP_NOT_FOUND) {
            $message = $this->t('You may not have permission to access the "android-class-insert" menu.');
            \Drupal::logger('digital_wallet_client')->error($message);
          }
        }
      }
      else {
        try {
          $response_json = $client->post($ws_url . '/android-class-update', [
            'headers' => $headers,
            'form_params' => ['data' => $encoder->encode($request, 'json')],
          ])->getBody()->getContents();

          $response = (array) \GuzzleHttp\json_decode($response_json);

          if ($response['status'] == StatusCodes::HTTP_OK) {
            drupal_set_message(t('"@title" updated on Google successfully.', ['@title' => $values['digital_wallet_client_loyaltyclass_title']]));
          }
          elseif ($response['status'] == StatusCodes::HTTP_BAD_REQUEST) {
            if ($response['reason'] == 'resourceNotFound') {
              $form_state->setErrorByName('digital_wallet_client_loyaltyclass_class_id', t('Class ID "@class" do not exists on Google server.', ['@class' => $values['digital_wallet_client_loyaltyclass_class_id']]));
            }
          }
          else {
            $form_state->setErrorByName('digital_wallet_client_loyaltyclass_class_id', t('Android class "@class" couldn\'t be updated on Google. Please try again.', ['@class' => $values['digital_wallet_client_loyaltyclass_class_id']]));
          }
        }
        catch (ClientException $e) {
          if ($e->getCode() == StatusCodes::HTTP_NOT_FOUND) {
            $message = $this->t('You may not have permission to access the "android-class-update" menu.');
            \Drupal::logger('digital_wallet_client')->error($message);
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $walletlayout = $this->entity;

    $layout_instance = $this->androidClassManager->createInstance($walletlayout->type);
    if (!$this->exist($walletlayout->id) || empty($walletlayout->type)) {
      $layout_instance->configFormSubmit($form, $form_state, $walletlayout, TRUE);
    }
    else {
      $walletlayout = $layout_instance->configFormSubmit($form, $form_state, $walletlayout);
      $walletlayout->updated = time();

    }

    $status = $walletlayout->save();

    if ($status) {
      drupal_set_message($this->t('Saved the %label Android Class.', [
        '%label' => $walletlayout->label(),
      ]));
    }
    else {
      drupal_set_message($this->t('The %label Android Class was not saved.', [
        '%label' => $walletlayout->label(),
      ]));
    }

    $form_state->setRedirect('entity.androidclass.collection');
  }

  /**
   * Helper function to check whether an Wallet Layout configuration entity exists.
   */
  public function exist($id) {
    $entity = $this->entityQuery->get('androidclass')
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
