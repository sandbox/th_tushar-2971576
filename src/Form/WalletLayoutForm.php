<?php

namespace Drupal\digital_wallet_client\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Form\FormStateInterface;
use Drupal\digital_wallet_client\WalletLayoutManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * Form handler for the Example add and edit forms.
 */
class WalletLayoutForm extends EntityForm {

  /**
   * Wallet Layout Manager.
   *
   * @var \Drupal\digital_wallet_client\WalletLayoutManager
   */
  protected $walletLayoutManager;

  /**
   * Serializer Component.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * Constructs an Wallet Layout Config Form object.
   *
   * @param \Drupal\Core\Entity\Query\QueryFactory $entity_query
   *   The entity query.
   * @param \Drupal\digital_wallet_client\WalletLayoutManager $wallet_layout_manager
   *   Wallet layout manager.
   * @param \Symfony\Component\Serializer\Serializer $serializer
   *   Serializer component.
   */
  public function __construct(QueryFactory $entity_query, WalletLayoutManager $wallet_layout_manager, Serializer $serializer) {
    $this->entityQuery = $entity_query;
    $this->walletLayoutManager = $wallet_layout_manager;
    $this->serializer = $serializer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.query'),
      $container->get('digital_wallet_client.manager.wallet_layout'),
      $container->get('serializer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $walletlayout = $this->entity;

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $walletlayout->title,
      '#description' => $this->t("The administrative title of the wallet layout."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $walletlayout->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$walletlayout->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => t('Enabled?'),
      '#default_value' => isset($walletlayout->status) ? $walletlayout->status : '',
      '#description' => t('Whether the layout is enabled / disabled.'),
      '#weight' => 9,
    ];

    if (!$this->exist($walletlayout->id) || empty($walletlayout->type)) {
      $plugin_definitions = $this->walletLayoutManager->getDefinitions();
      $walletlayout_options = [];
      foreach ($plugin_definitions as $id => $plugin) {
        $walletlayout_options[$id] = $plugin['admintitle']->render();
      }

      $current_user = \Drupal::currentUser();
      $form['type'] = [
        '#type' => 'select',
        '#title' => t('Layout Type'),
        '#options' => $walletlayout_options,
        '#default_value' => isset($walletlayout->type) ? $walletlayout->type : '',
        '#description' => t('Type of Wallet Layout.'),
        '#weight' => 8,
      ];

      $form['platform'] = [
        '#type' => 'hidden',
        '#value' => $plugin['platform']->render(),
      ];

      $form['uid'] = [
        '#type' => 'hidden',
        '#value' => $current_user->id(),
      ];
    }
    else {
      $layout_instance = $this->walletLayoutManager->createInstance($walletlayout->type);
      $form += $layout_instance->configForm($walletlayout);
    }

    if (isset($walletlayout->type)) {
      $form['title']['#prefix'] = '<div class="color-pallete-parent"><div class="color-pallete">';

      $default_wallet_img = drupal_get_path('module', 'digital_wallet_client') . '/images/' . $walletlayout->type . '.png';

      $form['wallet_image'] = [
        '#markup' => '</div><div class="' . $walletlayout->type . '-img"><img src="/' . $default_wallet_img . '" /></div>',
        '#weight' => 9999,
      ];

      $form['#attached']['library'] = [
        'digital_wallet_client/wallet_' . $walletlayout->type,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $walletlayout = $this->entity;

    $layout_instance = $this->walletLayoutManager->createInstance($walletlayout->type);
    if (!$this->exist($walletlayout->id) || empty($walletlayout->type)) {
      $layout_instance->configFormSubmit($form, $form_state, $walletlayout, TRUE);
    }
    else {
      $walletlayout = $layout_instance->configFormSubmit($form, $form_state, $walletlayout);
      $walletlayout->updated = time();
    }

    $status = $walletlayout->save();

    if ($status) {
      drupal_set_message($this->t('Saved the %label Wallet Layout.', [
        '%label' => $walletlayout->label(),
      ]));
    }
    else {
      drupal_set_message($this->t('The %label Wallet Layout was not saved.', [
        '%label' => $walletlayout->label(),
      ]));
    }

    $form_state->setRedirect('entity.walletlayout.collection');
  }

  /**
   * Helper function to check whether an Wallet Layout configuration entity exists.
   */
  public function exist($id) {
    $entity = $this->entityQuery->get('walletlayout')
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
