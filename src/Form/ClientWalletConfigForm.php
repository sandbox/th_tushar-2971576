<?php

namespace Drupal\digital_wallet_client\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for client module.
 */
class ClientWalletConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'digital_wallet_client_wallet_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['digital_wallet_client.main_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $client_wallet_config = $this->config('digital_wallet_client.main_settings');

    $form['client_wallet_api_settings'] = [
      '#type' => 'details',
      '#title' => t('Main Wallet API settings'),
      '#open' => TRUE,
    ];

    $form['client_wallet_api_settings']['webservice_base_url'] = [
      '#type' => 'textfield',
      '#title' => t('Microservice Base URL'),
      '#default_value' => $client_wallet_config->get('webservice_base_url'),
      '#description' => t('The Wallet Webservice Endpoint Base URL. Avoid trailing slash.'),
      '#required' => TRUE,
    ];

    $form['client_wallet_api_settings']['site_base_url'] = [
      '#type' => 'textfield',
      '#title' => t("Site's Base URL"),
      '#default_value' => $client_wallet_config->get('site_base_url'),
      '#description' => t("Current site's Base URL. Avoid trailing slash."),
      '#required' => TRUE,
    ];

    $form['client_wallet_api_settings']['microservice_consumer_id'] = [
      '#type' => 'textfield',
      '#title' => t("Site's Microservice Consumer ID"),
      '#default_value' => $client_wallet_config->get('microservice_consumer_id'),
      '#description' => t("Provide the site's consumer ID as received from Microservice upon onboarding."),
      '#required' => TRUE,
    ];

    $form['client_wallet_api_settings']['oauth_clientid'] = [
      '#type' => 'textfield',
      '#title' => t('OAuth Client ID'),
      '#default_value' => $client_wallet_config->get('oauth_clientid'),
      '#description' => t('OAuth Client ID.'),
      '#required' => TRUE,
    ];

    $form['client_wallet_api_settings']['oauth_clientsecret'] = [
      '#type' => 'textfield',
      '#title' => t('OAuth Client Secret'),
      '#default_value' => $client_wallet_config->get('oauth_clientsecret'),
      '#description' => t('OAuth Client Secret.'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('digital_wallet_client.main_settings')
      ->set('webservice_base_url', $form_state->getValue('webservice_base_url'))
      ->set('site_base_url', $form_state->getValue('site_base_url'))
      ->set('microservice_consumer_id', $form_state->getValue('microservice_consumer_id'))
      ->set('oauth_clientid', $form_state->getValue('oauth_clientid'))
      ->set('oauth_clientsecret', $form_state->getValue('oauth_clientsecret'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
