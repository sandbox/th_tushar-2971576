<?php

namespace Drupal\digital_wallet_client;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an Layout entity.
 */
interface AndroidClassEntityInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.
}
