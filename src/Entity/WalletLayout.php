<?php

namespace Drupal\digital_wallet_client\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\digital_wallet_client\LayoutEntityInterface;

/**
 * Defines the Wallet Layout entity.
 *
 * @ConfigEntityType(
 *   id = "walletlayout",
 *   label = @Translation("Wallet Layout"),
 *   handlers = {
 *     "list_builder" = "Drupal\digital_wallet_client\Controller\DigitalWalletLayoutListBuilder",
 *     "form" = {
 *       "add" = "Drupal\digital_wallet_client\Form\WalletLayoutForm",
 *       "edit" = "Drupal\digital_wallet_client\Form\WalletLayoutForm",
 *       "delete" = "Drupal\digital_wallet_client\Form\WalletLayoutDeleteForm",
 *     }
 *   },
 *   config_prefix = "walletlayout",
 *   admin_permission = "administer digital wallet client",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/digital-wallet/layouts/{walletlayout}/edit",
 *     "delete-form" = "/admin/config/digital-wallet/layouts/{walletlayout}/delete",
 *   }
 * )
 */
class WalletLayout extends ConfigEntityBase implements LayoutEntityInterface {

  /**
   * The Wallet Layout ID.
   *
   * @var string
   */
  public $id;

  /**
   * The Wallet Layout label.
   *
   * @var string
   */
  public $title;

  /**
   * The Wallet Layout type.
   *
   * @var string
   */
  public $type;

  /**
   * The Wallet Layout platform.
   *
   * @var string
   */
  public $platform;

  /**
   * The Wallet Layout user's UID.
   *
   * @var string
   */
  public $uid;

  /**
   * The Wallet Layout status.
   *
   * @var string
   */
  public $status;

  /**
   * The Wallet Layout creation date.
   *
   * @var string
   */
  public $created;

  /**
   * The Wallet Layout updated date.
   *
   * @var string
   */
  public $updated;

  /**
   * The Wallet Layout related data.
   *
   * @var string
   */
  public $data;

}
