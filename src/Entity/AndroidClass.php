<?php

namespace Drupal\digital_wallet_client\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\digital_wallet_client\AndroidClassEntityInterface;

/**
 * Defines the Wallet Layout entity.
 *
 * @ConfigEntityType(
 *   id = "androidclass",
 *   label = @Translation("Android Class"),
 *   handlers = {
 *     "list_builder" = "Drupal\digital_wallet_client\Controller\DigitalWalletAndroidClassListBuilder",
 *     "form" = {
 *       "add" = "Drupal\digital_wallet_client\Form\AndroidClassForm",
 *       "edit" = "Drupal\digital_wallet_client\Form\AndroidClassForm",
 *       "delete" = "Drupal\digital_wallet_client\Form\AndroidClassDeleteForm",
 *     }
 *   },
 *   config_prefix = "androidclass",
 *   admin_permission = "administer digital wallet client",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/digital-wallet/android-classes/{androidclass}/edit",
 *     "delete-form" = "/admin/config/digital-wallet/android-classes/{androidclass}/delete",
 *   }
 * )
 */
class AndroidClass extends ConfigEntityBase implements AndroidClassEntityInterface {

  /**
   * The Wallet Layout ID.
   *
   * @var string
   */
  public $id;

  /**
   * The Wallet Layout label.
   *
   * @var string
   */
  public $title;

  /**
   * The Wallet Layout type.
   *
   * @var string
   */
  public $type;

  /**
   * The Wallet Layout platform.
   *
   * @var string
   */
  public $platform;

  /**
   * The Wallet Layout user's UID.
   *
   * @var string
   */
  public $uid;

  /**
   * The Wallet Layout status.
   *
   * @var string
   */
  public $status;

  /**
   * The Wallet Layout creation date.
   *
   * @var string
   */
  public $created;

  /**
   * The Wallet Layout updated date.
   *
   * @var string
   */
  public $updated;

  /**
   * The Wallet Layout related data.
   *
   * @var string
   */
  public $data;

}
