<?php

namespace Drupal\digital_wallet_client;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an Layout entity.
 */
interface LayoutEntityInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.
}
