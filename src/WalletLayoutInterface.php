<?php

namespace Drupal\digital_wallet_client;

use Drupal\Core\Form\FormStateInterface;
use Drupal\digital_wallet_client\Entity\WalletLayout;

/**
 * Defines the common interface for all Archiver classes.
 *
 * @see \Drupal\digital_wallet_client\WalletLayoutManager
 * @see \Drupal\digital_wallet_client\Annotation\WalletLayout
 * @see plugin_api
 */
interface WalletLayoutInterface {

  /**
   * Configuration form provider.
   *
   * @param \Drupal\digital_wallet_client\Entity\WalletLayout $walletlayout
   *   Wallet Layout Entity.
   *
   * @return array
   *   Wallet Layout Config Form.
   */
  public function configForm(WalletLayout $walletlayout);

  /**
   * Configuration form submit handler.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State Object.
   * @param \Drupal\digital_wallet_client\Entity\WalletLayout $walletLayout
   *   Wallet Layout.
   * @param bool $new
   *   Indicates whether entity is new.
   */
  public function configFormSubmit(array $form, FormStateInterface &$form_state, WalletLayout $walletLayout, $new = FALSE);

  /**
   * Provides the default data for layout.
   *
   * @return array
   *   Layout Default data.
   */
  public function defaultData();

  /**
   * Provides the raw data for layout.
   *
   * @param string $layout_id
   *   Layout ID.
   *
   * @return array
   *   Layout raw data.
   */
  public function rawData($layout_id);

  /**
   * Provides the data required for webservice request.
   *
   * @param string $layout_id
   *   Layout ID.
   * @param string $serial_number
   *   Serial Number.
   * @param string $group_number
   *   Group Number.
   *
   * @return mixed
   *   Layout request data.
   */
  public function requestData($layout_id, $serial_number, $group_number);

}
