<?php

namespace Drupal\digital_wallet_client\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller routines for wallet client.
 */
class DigitalWalletClientController extends ControllerBase {

  /**
   * Controller for providing the Client Wallet layout types.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   *
   * @return array
   *   Returns all layout types.
   */
  public function clientWalletLayoutTypes(Request $request) {
    $type = \Drupal::service('digital_wallet_client.manager.wallet_layout');
    $plugin_definitions = $type->getDefinitions();

    $header = [
      t('Name'),
      t('Administrative Title'),
      t('Platform'),
      t('Machine Name'),
    ];

    foreach ($plugin_definitions as $id => $plugin) {
      $rows[] = [
        $plugin['title']->render(),
        $plugin['admintitle']->render(),
        $plugin['platform']->render(),
        $plugin['id'],
      ];
    }

    $output = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No layout types available.'),
    ];

    return $output;
  }

  /**
   * Controller for providing the Client Wallet Android Classes.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   *
   * @return array
   *   Returns all available android classes.
   */
  public function clientWalletAndroidClasses(Request $request) {
    $type = \Drupal::service('digital_wallet_client.manager.android_class');
    $plugin_definitions = $type->getDefinitions();

    $header = [
      t('Name'),
      t('Administrative Title'),
      t('Platform'),
      t('Machine Name'),
    ];

    foreach ($plugin_definitions as $id => $plugin) {
      $rows[] = [
        $plugin['title']->render(),
        $plugin['admintitle']->render(),
        $plugin['platform']->render(),
        $plugin['id'],
      ];
    }

    $output = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No android classes available.'),
    ];

    return $output;
  }

}
