<?php

namespace Drupal\digital_wallet_client\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Controller for building a listing of Wallet Layout.
 */
class DigitalWalletAndroidClassListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['title'] = $this->t('Title');
    $header['id'] = $this->t('Unique ID');
    $header['type'] = $this->t('Type');
    $header['platform'] = $this->t('Platform');
    $header['status'] = $this->t('Status');
    $header['created'] = $this->t('Created');
    $header['changed'] = $this->t('Last Modified');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['title'] = $entity->title;
    $row['id'] = $entity->id();
    $row['type'] = $entity->type;
    $row['platform'] = $entity->platform;
    $row['status'] = $entity->status ? $this->t('Enabled') : $this->t('Disabled');
    $row['created'] = $entity->created ? date('d-m-Y H:i:s', $entity->created) : '';
    $row['changed'] = $entity->updated ? date('d-m-Y H:i:s', $entity->updated) : '';

    return $row + parent::buildRow($entity);
  }

}
