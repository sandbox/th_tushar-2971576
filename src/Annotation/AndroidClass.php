<?php

namespace Drupal\digital_wallet_client\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an android class annotation object.
 *
 * Plugin Namespace: Plugin\AndroidClass.
 *
 * @see \Drupal\digital_wallet_client\AndroidClassManager
 * @see \Drupal\digital_wallet_client\AndroidClassInterface
 * @see plugin_api
 * @see hook_android_class_info_alter()
 *
 * @Annotation
 */
class AndroidClass extends Plugin {

  /**
   * The android class plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the android class plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The admin title of the android class plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $admintitle;

  /**
   * The platform of the android class plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $platform;

  /**
   * The description of the android class plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
