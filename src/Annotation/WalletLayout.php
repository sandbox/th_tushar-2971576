<?php

namespace Drupal\digital_wallet_client\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an wallet layout annotation object.
 *
 * Plugin Namespace: Plugin\WalletLayout.
 *
 * @see \Drupal\digital_wallet_client\WalletLayoutManager
 * @see \Drupal\digital_wallet_client\WalletLayoutInterface
 * @see plugin_api
 * @see hook_wallet_layout_info_alter()
 *
 * @Annotation
 */
class WalletLayout extends Plugin {

  /**
   * The wallet layout plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the wallet layout plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The admin title of the wallet layout plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $admintitle;

  /**
   * The platform of the wallet layout plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $platform;

  /**
   * The description of the wallet layout plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
