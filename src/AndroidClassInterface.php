<?php

namespace Drupal\digital_wallet_client;

use Drupal\digital_wallet_client\Entity\AndroidClass;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the common interface for all Archiver classes.
 *
 * @see \Drupal\digital_wallet_client\AndroidClassManager
 * @see \Drupal\digital_wallet_client\Annotation\AndroidClass
 * @see plugin_api
 */
interface AndroidClassInterface {

  /**
   * Configuration form provider.
   *
   * @param \Drupal\digital_wallet_client\Entity\AndroidClass $androidclass
   *   Android Class.
   *
   * @return array
   *   Android class config form.
   */
  public function configForm(AndroidClass $androidclass);

  /**
   * Configuration form submit handler.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State Object.
   * @param \Drupal\digital_wallet_client\Entity\AndroidClass $androidclass
   *   Android Class.
   * @param bool $new
   *   Indicates whether entity is new.
   */
  public function configFormSubmit(array $form, FormStateInterface &$form_state, AndroidClass $androidclass, $new = FALSE);

  /**
   * Provides the default data for layout.
   *
   * @return array
   *   Android Class default data.
   */
  public function defaultData();

  /**
   * Provides the raw data for layout.
   *
   * @param string $layout_id
   *   Layout ID.
   *
   * @return array
   *   Android Class raw data.
   */
  public function rawData($layout_id);

  /**
   * Provides the data required for webservice request.
   *
   * @param string $layout_id
   *   Layout ID.
   *
   * @return mixed
   *   Android Class request data.
   */
  public function requestData($layout_id);

}
