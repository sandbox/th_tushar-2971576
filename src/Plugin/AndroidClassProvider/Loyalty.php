<?php

namespace Drupal\digital_wallet_client\Plugin\AndroidClassProvider;

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\digital_wallet_client\AndroidClassInterface;
use Drupal\digital_wallet_client\Entity\AndroidClass;
use Drupal\digital_wallet_client\StatusCodes;

/**
 * Defines a android loyalty layout interface.
 *
 * @AndroidClass(
 *   id = "loyaltyclass",
 *   title = @Translation("Loyalty Class"),
 *   admintitle = @Translation("Loyalty Class for Android"),
 *   platform = @Translation("Android"),
 *   description = @Translation("This is a default class type supported by Android."),
 * )
 */
class Loyalty implements AndroidClassInterface {

  /**
   * Android Class config form.
   *
   * @param \Drupal\digital_wallet_client\Entity\AndroidClass $androidclass
   *   AndroidClass object.
   *
   * @return array
   *   Config form.
   */
  public function configForm(AndroidClass $androidclass) {
    $encoder = \Drupal::getContainer()->get('serializer.encoder.json');
    $androidclass_data = $encoder->decode($androidclass->data, 'json');

    $androidclass = (array) $androidclass;

    if (!empty($androidclass_data['data']['id'])) {
      $class_id_disabled = TRUE;
    }
    else {
      $class_id_disabled = FALSE;
    }
    $form['digital_wallet_client_loyaltyclass_title'] = [
      '#type' => 'textfield',
      '#title' => t('Class Title'),
      '#default_value' => isset($androidclass['title']) ? $androidclass['title'] : '',
      '#description' => t('The administrative title of the wallet androidclass.'),
      '#required' => TRUE,
      '#description' => t('Title for this class.'),
    ];

    $form['digital_wallet_client_loyaltyclass_class_id'] = [
      '#type' => 'textfield',
      '#title' => t('Class ID'),
      '#default_value' => isset($androidclass_data['data']['id']) ? $androidclass_data['data']['id'] : '',
      '#required' => TRUE,
      '#disabled' => $class_id_disabled,
      '#description' => t('Unique Class ID. This field is stored on Google server.'),
    ];

    $form['digital_wallet_client_loyaltyclass_progname'] = [
      '#type' => 'textfield',
      '#title' => t('Program Name'),
      '#default_value' => isset($androidclass_data['data']['program_name']) ? $androidclass_data['data']['program_name'] : '',
      '#required' => TRUE,
      '#description' => t('Program Name or Site Name to be displayed in Google Pay app.'),
    ];

    $form['digital_wallet_client_loyaltyclass_issuername'] = [
      '#type' => 'textfield',
      '#title' => t('Issuer Name'),
      '#default_value' => isset($androidclass_data['data']['issuer_name']) ? $androidclass_data['data']['issuer_name'] : '',
      '#required' => TRUE,
      '#description' => t('Issuer Name. This field can be an organization name by default.'),
    ];

    $form['digital_wallet_client_loyaltyclass_logo'] = [
      '#type' => 'managed_file',
      '#title' => t('Program Logo'),
      '#default_value' => isset($androidclass_data['data']['program_logo']) ? $androidclass_data['data']['program_logo'] : '',
      '#upload_location' => 'public://wallet_images/',
      '#upload_validators' => ['file_validate_extensions' => ['png']],
      '#required' => TRUE,
      '#description' => t('Image to be displayed as a logo in Google Pay app.'),
    ];

    if (isset($androidclass_data['data']['program_logo']) && !empty($androidclass_data['data']['program_logo'])) {
      $form['digital_wallet_client_loyaltyclass_logo']['#description'] = t('Image to be displayed as a logo in Google Pay app. Field Value: @value', ['@value' => print_r($androidclass_data['data']['program_logo'], TRUE)]);
    }

    $form['digital_wallet_client_loyaltyclass_heroimage'] = [
      '#type' => 'managed_file',
      '#title' => t('Hero Image'),
      '#default_value' => isset($androidclass_data['data']['hero_image']) ? $androidclass_data['data']['hero_image'] : '',
      '#upload_location' => 'public://wallet_images/',
      '#upload_validators' => ['file_validate_extensions' => ['png']],
      '#description' => t('Image to be displayed as a hero image in Google Pay app.'),
    ];

    if (isset($androidclass_data['data']['hero_image']) && !empty($androidclass_data['data']['hero_image'])) {
      $form['digital_wallet_client_loyaltyclass_heroimage']['#description'] = t('Image to be displayed as a hero image in Google Pay app. Field Value: @value', ['@value' => print_r($androidclass_data['data']['hero_image'], TRUE)]);
    }

    $form['digital_wallet_client_loyaltyclass_accountidlabel'] = [
      '#type' => 'textfield',
      '#title' => t('Account ID Label'),
      '#default_value' => isset($androidclass_data['data']['account_id_label']) ? $androidclass_data['data']['account_id_label'] : 'Card Number',
      '#description' => t('Text to be displayed in Account ID Label placeholder. This field can be used to display Card Number in App.'),
    ];

    $form['digital_wallet_client_loyaltyclass_accountnamelabel'] = [
      '#type' => 'textfield',
      '#title' => t('Account Name Label'),
      '#default_value' => isset($androidclass_data['data']['account_name_label']) ? $androidclass_data['data']['account_name_label'] : 'Group Number',
      '#description' => t('Text to be displayed in Account Name Label placeholder. This field can be used to display Group Number in App.'),
    ];

    $form['digital_wallet_client_loyaltyclass_backcolor'] = [
      '#type' => 'jquery_colorpicker',
      '#title' => t('Background Color (Hex Format)'),
      '#default_value' => isset($androidclass_data['data']['backgroundcolor']) ? $androidclass_data['data']['backgroundcolor'] : '',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * Submit Handler.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   * @param \Drupal\digital_wallet_client\Entity\AndroidClass $androidclass
   *   Android Class object.
   * @param bool $new
   *   Indicates whether entity is new.
   */
  public function configFormSubmit(array $form, FormStateInterface &$form_state, AndroidClass $androidclass, $new = FALSE) {
    $encoder = \Drupal::service('serializer.encoder.json');
    if ($new) {
      $default_data = $this->defaultData();
      $androidclass->set('created', time());
      $androidclass->data = $encoder->encode($default_data, 'json');
    }
    else {
      $values = $form_state->getValues();
      $androidclassdata = $encoder->decode($androidclass->data, 'json');

      if (is_numeric($values['digital_wallet_client_loyaltyclass_logo']['0']) && !empty($values['digital_wallet_client_loyaltyclass_logo']['0'])) {
        $loyaltyclass_logo_file = File::load($values['digital_wallet_client_loyaltyclass_logo']['0']);
        if ($loyaltyclass_logo_file->isTemporary()) {
          $loyaltyclass_logo_file->setPermanent();
          $loyaltyclass_logo_file->save();
        }
      }

      $androidclassdata['data']['id'] = $values['digital_wallet_client_loyaltyclass_class_id'];
      $androidclassdata['data']['program_name'] = $values['digital_wallet_client_loyaltyclass_progname'];
      $androidclassdata['data']['issuer_name'] = $values['digital_wallet_client_loyaltyclass_issuername'];
      $androidclassdata['data']['program_logo'] = $values['digital_wallet_client_loyaltyclass_logo'];
      $androidclassdata['data']['account_id_label'] = $values['digital_wallet_client_loyaltyclass_accountidlabel'];
      $androidclassdata['data']['account_name_label'] = $values['digital_wallet_client_loyaltyclass_accountnamelabel'];
      $androidclassdata['data']['backgroundcolor'] = $values['digital_wallet_client_loyaltyclass_backcolor'];

      if (!empty($values['digital_wallet_client_loyaltyclass_heroimage'])) {
        if (is_numeric($values['digital_wallet_client_loyaltyclass_heroimage']['0']) && $values['digital_wallet_client_loyaltyclass_heroimage']['0']) {
          $loyaltyclass_hero_file = File::load($values['digital_wallet_client_loyaltyclass_heroimage']['0']);
          if ($loyaltyclass_hero_file->isTemporary()) {
            $loyaltyclass_hero_file->setPermanent();
            $loyaltyclass_hero_file->save();
          }

          $androidclassdata['data']['hero_image'] = $values['digital_wallet_client_loyaltyclass_heroimage'];
        }
        else {
          unset($androidclassdata['data']['hero_image']);
        }
      }
      else {
        unset($androidclassdata['data']['hero_image']);
      }

      $androidclass->data = $encoder->encode($androidclassdata, 'json');
    }

    return $androidclass;
  }

  /**
   * Callback for providing the default data for layout.
   *
   * @return array|mixed
   *   The default data structure that is required for wallet webservice request.
   */
  public function defaultData() {
    $defaults['data'] = [
      'id' => '',
      'program_name' => '',
      'issuer_name' => '',
      'program_logo' => '',
      'account_id_label' => '',
      'account_name_label' => '',
      'backgroundcolor' => '',
    ];

    return $defaults;
  }

  /**
   * Callback for providing the raw data for this class.
   *
   * @param string $layout_id
   *   The unique identifier for android class.
   *
   * @return array
   *   The raw data of the layout.
   */
  public function rawData($layout_id) {
    $layout_data = [];

    $layout_entity = \Drupal::entityTypeManager()->getStorage('walletlayout')->load($layout_id);
    $encoder = \Drupal::service('serializer.encoder.json');

    if ($layout_entity instanceof WalletLayout) {
      $layout_data = $encoder->decode($layout_entity->data, 'json');
    }

    return $layout_data;
  }

  /**
   * Request Data.
   *
   * @param string $layout_id
   *   Layout ID.
   *
   * @return array|mixed
   *   Android Request Data.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function requestData($layout_id) {
    $layout_data = [];

    $layout_entity = \Drupal::entityTypeManager()->getStorage('walletlayout')->load($layout_id);
    $encoder = \Drupal::service('serializer.encoder.json');

    if ($layout_entity instanceof WalletLayout) {
      $layout_data['data'] = $encoder->decode($layout_entity->data, 'json');

      $layout_data['data']['backgroundcolor'] = '#' . $layout_data['data']['backgroundcolor'];
      $layout_data['status'] = StatusCodes::HTTP_OK;
    }
    else {
      $layout_data = [
        'data' => [],
        'status' => StatusCodes::HTTP_FORBIDDEN,
      ];
    }

    return $layout_data;
  }

}
