<?php

namespace Drupal\digital_wallet_client\Plugin\WalletLayoutProvider;

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\file\Plugin\views\filter\Status;
use Drupal\digital_wallet_client\Entity\WalletLayout;
use Drupal\digital_wallet_client\StatusCodes;
use Drupal\digital_wallet_client\WalletLayoutInterface;

/**
 * Defines a apple generic layout interface.
 *
 * @WalletLayout(
 *   id = "generic",
 *   title = @Translation("Generic"),
 *   admintitle = @Translation("Generic layout for Apple Wallet"),
 *   platform = @Translation("Apple"),
 *   description = @Translation("This is a default generic type supported by Apple Wallet PKPass library."),
 * )
 */
class AppleGeneric implements WalletLayoutInterface {

  /**
   * Config form for layout.
   *
   * @param \Drupal\digital_wallet_client\Entity\WalletLayout $walletlayout
   *   Wallet Layout Entity.
   *
   * @return array
   *   Layout Config form.
   */
  public function configForm(WalletLayout $walletlayout) {
    $encoder = \Drupal::getContainer()->get('serializer.encoder.json');
    $layout_data = $encoder->decode($walletlayout->data, 'json');

    $form['digital_wallet_client_generic_data'] = [
      '#type' => 'details',
      '#title' => t('Data to be displayed in generic layout'),
      '#weight' => 2,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_generic_logotext'] = [
      '#type' => 'textfield',
      '#title' => t('Logo Text'),
      '#default_value' => isset($layout_data['data']['logoText']) ? $layout_data['data']['logoText'] : '',
      '#description' => t('This data will be displayed as text beside logo in wallet.'),
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_generic_description'] = [
      '#type' => 'textfield',
      '#title' => t('Layout Description'),
      '#default_value' => isset($layout_data['data']['description']) ? $layout_data['data']['description'] : '',
      '#description' => t('This data will be displayed as title of the card in wallet. Don’t try to include all of the data on the pass in its description, just include enough detail to distinguish passes of the same type.'),
      '#required' => TRUE,
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_generic_background'] = [
      '#type' => 'jquery_colorpicker',
      '#title' => t('Background Color (RGB Format)'),
      '#default_value' => isset($layout_data['data']['backgroundColor']) ? $layout_data['data']['backgroundColor'] : '',
      '#description' => t('This data is the Background Color of the generic layout in wallet.'),
      '#required' => TRUE,
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_generic_foreground'] = [
      '#type' => 'jquery_colorpicker',
      '#title' => t('Foreground Text Color (RGB Format)'),
      '#default_value' => isset($layout_data['data']['foregroundColor']) ? $layout_data['data']['foregroundColor'] : '',
      '#description' => t('This data is the Foreground Color of the generic layout in wallet.'),
      '#required' => TRUE,
    ];

    $module_path = drupal_get_path('module', 'digital_wallet_client');
    $generic_img = $module_path . '/images/generic.png';
    $form['digital_wallet_client_generic_data']['digital_wallet_client_generic_labelcolor'] = [
      '#type' => 'jquery_colorpicker',
      '#title' => t('Label Text Color (RGB Format)'),
      '#default_value' => isset($layout_data['data']['labelColor']) ? $layout_data['data']['labelColor'] : '',
      '#description' => t('This data is the Label Color of the generic layout in wallet.'),
      '#required' => TRUE,
    ];

    $barcode_options = [
      'None' => t('None'),
      'PKBarcodeFormatQR' => t('PKBarcodeFormatQR'),
      'PKBarcodeFormatPDF417' => t('PKBarcodeFormatPDF417'),
      'PKBarcodeFormatAztec' => t('PKBarcodeFormatAztec'),
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_generic_barcode'] = [
      '#type' => 'select',
      '#title' => t('Barcode Type'),
      '#default_value' => isset($layout_data['barcode_type']) ? $layout_data['barcode_type'] : '',
      '#description' => t('The type of barcode to be displayed for this generic layout in wallet.'),
      '#options' => $barcode_options,
      '#required' => TRUE,
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_generic_icon'] = [
      '#type' => 'managed_file',
      '#title' => t('Icon'),
      '#description' => t('Upload the image for generic layout icon.'),
      '#default_value' => isset($layout_data['files']['icon']) ? $layout_data['files']['icon'] : '',
      '#upload_location' => 'public://wallet_images/',
      '#upload_validators' => ['file_validate_extensions' => ['png']],
      '#required' => TRUE,
    ];

    if (isset($layout_data['files']['icon']) && !empty($layout_data['files']['icon'])) {
      $form['digital_wallet_client_generic_data']['digital_wallet_client_generic_icon']['#description'] = t('Upload the image for generic layout icon. Field Value: @value', ['@value' => print_r($layout_data['files']['icon'], TRUE)]);
    }

    $form['digital_wallet_client_generic_data']['digital_wallet_client_generic_icon_2x'] = [
      '#type' => 'managed_file',
      '#title' => t('Icon (2x)'),
      '#description' => t('Upload the image for generic layout icon (2x).'),
      '#default_value' => isset($layout_data['files']['icon2x']) ? $layout_data['files']['icon2x'] : '',
      '#upload_location' => 'public://wallet_images/',
      '#upload_validators' => ['file_validate_extensions' => ['png']],
      '#required' => TRUE,
    ];

    if (isset($layout_data['files']['icon2x']) && !empty($layout_data['files']['icon2x'])) {
      $form['digital_wallet_client_generic_data']['digital_wallet_client_generic_icon_2x']['#description'] = t('Upload the image for generic layout icon (2x). Field Value: @value', ['@value' => print_r($layout_data['files']['icon2x'], TRUE)]);
    }

    $form['digital_wallet_client_generic_data']['digital_wallet_client_generic_logo'] = [
      '#type' => 'managed_file',
      '#title' => t('Logo'),
      '#description' => t('Upload the image for generic layout logo.'),
      '#default_value' => isset($layout_data['files']['logo']) ? $layout_data['files']['logo'] : '',
      '#upload_location' => 'public://wallet_images/',
      '#upload_validators' => ['file_validate_extensions' => ['png']],
      '#required' => TRUE,
    ];

    if (isset($layout_data['files']['logo']) && !empty($layout_data['files']['logo'])) {
      $form['digital_wallet_client_generic_data']['digital_wallet_client_generic_logo']['#description'] = t('Upload the image for generic layout logo. Field Value: @value', ['@value' => print_r($layout_data['files']['logo'], TRUE)]);
    }

    $form['digital_wallet_client_generic_data']['digital_wallet_client_generic_thumb'] = [
      '#type' => 'managed_file',
      '#title' => t('Thumbnail'),
      '#description' => t('Upload the image for generic layout thumbnail.'),
      '#default_value' => isset($layout_data['files']['thumbnail']) ? $layout_data['files']['thumbnail'] : '',
      '#upload_location' => 'public://wallet_images/',
      '#upload_validators' => ['file_validate_extensions' => ['png']],
    ];

    if (isset($layout_data['files']['thumbnail']) && !empty($layout_data['files']['thumbnail'])) {
      $form['digital_wallet_client_generic_data']['digital_wallet_client_generic_thumb']['#description'] = t('Upload the image for generic layout thumbnail. Field Value: @value', ['@value' => print_r($layout_data['files']['thumbnail'], TRUE)]);
    }

    $form['digital_wallet_client_generic_data']['digital_wallet_client_generic_thumb2x'] = [
      '#type' => 'managed_file',
      '#title' => t('Thumbnail (2x)'),
      '#description' => t('Upload the image for generic layout thumbnail (2x).'),
      '#default_value' => isset($layout_data['files']['thumbnail2x']) ? $layout_data['files']['thumbnail2x'] : '',
      '#upload_location' => 'public://wallet_images/',
      '#upload_validators' => ['file_validate_extensions' => ['png']],
    ];

    if (isset($layout_data['files']['thumbnail2x']) && !empty($layout_data['files']['thumbnail2x'])) {
      $form['digital_wallet_client_generic_data']['digital_wallet_client_generic_thumb2x']['#description'] = t('Upload the image for generic layout thumbnail (2x). Field Value: @value', ['@value' => print_r($layout_data['files']['thumbnail2x'], TRUE)]);
    }

    $form['digital_wallet_client_generic_data']['digital_wallet_client_primaryfields'] = [
      '#type' => 'details',
      '#title' => t('Primary Fields'),
      '#weight' => 1,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('This data will be displayed in the primary fields section in wallet.'),
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_primaryfields']['digital_wallet_client_primaryfields_label'] = [
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#default_value' => isset($layout_data['data']['generic']['primaryFields']['0']['label']) ? $layout_data['data']['generic']['primaryFields']['0']['label'] : '',
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_primaryfields']['digital_wallet_client_primaryfields_value'] = [
      '#type' => 'textfield',
      '#title' => t('Value'),
      '#default_value' => isset($layout_data['data']['generic']['primaryFields']['0']['value']) ? $layout_data['data']['generic']['primaryFields']['0']['value'] : '',
    ];

    $text_alignment_options = [
      'PKTextAlignmentLeft' => t('PKTextAlignmentLeft'),
      'PKTextAlignmentCenter' => t('PKTextAlignmentCenter'),
      'PKTextAlignmentRight' => t('PKTextAlignmentRight'),
      'PKTextAlignmentNatural' => t('PKTextAlignmentNatural'),
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_secfields1'] = [
      '#type' => 'details',
      '#title' => t('Secondary Field 1'),
      '#weight' => 2,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('This data will be displayed in the secondary fields section in wallet.'),
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_secfields1']['digital_wallet_client_secfields1_label'] = [
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#default_value' => isset($layout_data['data']['generic']['secondaryFields']['0']['label']) ? $layout_data['data']['generic']['secondaryFields']['0']['label'] : '',
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_secfields1']['digital_wallet_client_secfields1_value'] = [
      '#type' => 'textfield',
      '#title' => t('Value'),
      '#default_value' => isset($layout_data['data']['generic']['secondaryFields']['0']['value']) ? $layout_data['data']['generic']['secondaryFields']['0']['value'] : '',
      '#description' => t('Token for this field is @value. Keep value of this field as token to pass dynamic value to this field. Note, data of this field will be passed to Barcode field.', ['@value' => '@cardnumber']),
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_secfields1']['digital_wallet_client_secfields1_align'] = [
      '#type' => 'select',
      '#title' => t('Text Align'),
      '#default_value' => isset($layout_data['data']['generic']['secondaryFields']['0']['textAlignment']) ? $layout_data['data']['generic']['secondaryFields']['0']['textAlignment'] : '',
      '#options' => $text_alignment_options,
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_secfields2'] = [
      '#type' => 'details',
      '#title' => t('Secondary Field 2'),
      '#weight' => 3,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('This data will be displayed in the auxiliary fields section in wallet.'),
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_secfields2']['digital_wallet_client_secfields2_label'] = [
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#default_value' => isset($layout_data['data']['generic']['secondaryFields']['1']['label']) ? $layout_data['data']['generic']['secondaryFields']['1']['label'] : '',
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_secfields2']['digital_wallet_client_secfields2_value'] = [
      '#type' => 'textfield',
      '#title' => t('Value'),
      '#default_value' => isset($layout_data['data']['generic']['secondaryFields']['1']['value']) ? $layout_data['data']['generic']['secondaryFields']['1']['value'] : '',
      '#description' => t('Token for this field is @value. Keep value of this field as token to pass dynamic value to this field.', ['@value' => '@groupnumber']),
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_secfields2']['digital_wallet_client_secfields2_align'] = [
      '#type' => 'select',
      '#title' => t('Text Align'),
      '#default_value' => isset($layout_data['data']['generic']['secondaryFields']['1']['textAlignment']) ? $layout_data['data']['generic']['secondaryFields']['1']['textAlignment'] : '',
      '#options' => $text_alignment_options,
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_auxfields1'] = [
      '#type' => 'details',
      '#title' => t('Auxiliary Field 1'),
      '#weight' => 4,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('This data will be displayed in the auxiliary fields section in wallet.'),
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_auxfields1']['digital_wallet_client_auxfields1_label'] = [
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#default_value' => isset($layout_data['data']['generic']['auxiliaryFields']['0']['label']) ? $layout_data['data']['generic']['auxiliaryFields']['0']['label'] : '',
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_auxfields1']['digital_wallet_client_auxfields1_value'] = [
      '#type' => 'textfield',
      '#title' => t('Value'),
      '#default_value' => isset($layout_data['data']['generic']['auxiliaryFields']['0']['value']) ? $layout_data['data']['generic']['auxiliaryFields']['0']['value'] : '',
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_auxfields1']['digital_wallet_client_auxfields1_align'] = [
      '#type' => 'select',
      '#title' => t('Text Align'),
      '#default_value' => isset($layout_data['data']['generic']['auxiliaryFields']['0']['textAlignment']) ? $layout_data['data']['generic']['auxiliaryFields']['0']['textAlignment'] : '',
      '#options' => $text_alignment_options,
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_auxfields2'] = [
      '#type' => 'details',
      '#title' => t('Auxiliary Field 2'),
      '#weight' => 5,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('This data will be displayed in the auxiliary fields section in wallet.'),
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_auxfields2']['digital_wallet_client_auxfields2_label'] = [
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#default_value' => isset($layout_data['data']['generic']['auxiliaryFields']['1']['label']) ? $layout_data['data']['generic']['auxiliaryFields']['1']['label'] : '',
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_auxfields2']['digital_wallet_client_auxfields2_value'] = [
      '#type' => 'textfield',
      '#title' => t('Value'),
      '#default_value' => isset($layout_data['data']['generic']['auxiliaryFields']['1']['value']) ? $layout_data['data']['generic']['auxiliaryFields']['1']['value'] : '',
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_auxfields2']['digital_wallet_client_auxfields2_align'] = [
      '#type' => 'select',
      '#title' => t('Text Align'),
      '#default_value' => isset($layout_data['data']['generic']['auxiliaryFields']['1']['textAlignment']) ? $layout_data['data']['generic']['auxiliaryFields']['1']['textAlignment'] : '',
      '#options' => $text_alignment_options,
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_backfield1'] = [
      '#type' => 'details',
      '#title' => t('Backfield Data 1'),
      '#weight' => 6,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('This data will be displayed behind the card in wallet.'),
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_backfield1']['digital_wallet_client_backfield1_label'] = [
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#default_value' => isset($layout_data['data']['generic']['backFields']['0']['label']) ? $layout_data['data']['generic']['backFields']['0']['label'] : '',
      '#required' => TRUE,
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_backfield1']['digital_wallet_client_backfield1_value'] = [
      '#type' => 'textarea',
      '#title' => t('Value'),
      '#default_value' => isset($layout_data['data']['generic']['backFields']['0']['value']) ? $layout_data['data']['generic']['backFields']['0']['value'] : '',
      '#required' => TRUE,
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_backfield2'] = [
      '#type' => 'details',
      '#title' => t('Backfield Data 2'),
      '#weight' => 7,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('This data will be displayed behind the card in wallet.'),
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_backfield2']['digital_wallet_client_backfield2_label'] = [
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#default_value' => isset($layout_data['data']['generic']['backFields']['1']['label']) ? $layout_data['data']['generic']['backFields']['1']['label'] : '',
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_backfield2']['digital_wallet_client_backfield2_value'] = [
      '#type' => 'textarea',
      '#title' => t('Value'),
      '#default_value' => isset($layout_data['data']['generic']['backFields']['1']['value']) ? $layout_data['data']['generic']['backFields']['1']['value'] : '',
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_backfield3'] = [
      '#type' => 'details',
      '#title' => t('Backfield Data 3'),
      '#weight' => 8,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('This data will be displayed behind the card in wallet.'),
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_backfield3']['digital_wallet_client_backfield3_label'] = [
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#default_value' => isset($layout_data['data']['generic']['backFields']['2']['label']) ? $layout_data['data']['generic']['backFields']['2']['label'] : '',
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_backfield3']['digital_wallet_client_backfield3_value'] = [
      '#type' => 'textarea',
      '#title' => t('Value'),
      '#default_value' => isset($layout_data['data']['generic']['backFields']['2']['value']) ? $layout_data['data']['generic']['backFields']['2']['value'] : '',
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_backfield4'] = [
      '#type' => 'details',
      '#title' => t('Backfield Data 4'),
      '#weight' => 9,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('This data will be displayed behind the card in wallet.'),
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_backfield4']['digital_wallet_client_backfield4_label'] = [
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#default_value' => isset($layout_data['data']['generic']['backFields']['3']['label']) ? $layout_data['data']['generic']['backFields']['3']['label'] : '',
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_backfield4']['digital_wallet_client_backfield4_value'] = [
      '#type' => 'textarea',
      '#title' => t('Value'),
      '#default_value' => isset($layout_data['data']['generic']['backFields']['3']['value']) ? $layout_data['data']['generic']['backFields']['3']['value'] : '',
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_backfield5'] = [
      '#type' => 'details',
      '#title' => t('Backfield Data 5'),
      '#weight' => 10,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('This data will be displayed behind the card in wallet.'),
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_backfield5']['digital_wallet_client_backfield5_label'] = [
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#default_value' => isset($layout_data['data']['generic']['backFields']['4']['label']) ? $layout_data['data']['generic']['backFields']['4']['label'] : '',
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_backfield5']['digital_wallet_client_backfield5_value'] = [
      '#type' => 'textarea',
      '#title' => t('Value'),
      '#default_value' => isset($layout_data['data']['generic']['backFields']['4']['value']) ? $layout_data['data']['generic']['backFields']['4']['value'] : '',
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_backfield6'] = [
      '#type' => 'details',
      '#title' => t('Backfield Data 6'),
      '#weight' => 11,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('This data will be displayed behind the card in wallet.'),
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_backfield6']['digital_wallet_client_backfield6_label'] = [
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#default_value' => isset($layout_data['data']['generic']['backFields']['5']['label']) ? $layout_data['data']['generic']['backFields']['5']['label'] : '',
    ];

    $form['digital_wallet_client_generic_data']['digital_wallet_client_backfield6']['digital_wallet_client_backfield6_value'] = [
      '#type' => 'textarea',
      '#title' => t('Value'),
      '#default_value' => isset($layout_data['data']['generic']['backFields']['5']['value']) ? $layout_data['data']['generic']['backFields']['5']['value'] : '',
    ];

    return $form;
  }

  /**
   * Config form submit.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   * @param \Drupal\digital_wallet_client\Entity\WalletLayout $walletlayout
   *   Wallet Layout Entity.
   * @param bool $new
   *   Indicates if entity is new.
   */
  public function configFormSubmit(array $form, FormStateInterface &$form_state, WalletLayout $walletlayout, $new = FALSE) {
    $encoder = \Drupal::service('serializer.encoder.json');
    $walletlayout->set('platform', 'Apple');

    if ($new) {
      $default_data = $this->defaultData();
      $walletlayout->set('created', time());
      $walletlayout->data = $encoder->encode($default_data, 'json');
    }
    else {
      $values = $form_state->getValues();
      $walletlayoutdata = $encoder->decode($walletlayout->data, 'json');

      if (!empty($values['digital_wallet_client_generic_logotext'])) {
        $walletlayoutdata['data']['logoText'] = $values['digital_wallet_client_generic_logotext'];
      }
      else {
        unset($walletlayoutdata['data']['logoText']);
      }

      $walletlayoutdata['data']['description'] = $values['digital_wallet_client_generic_description'];
      $walletlayoutdata['data']['backgroundColor'] = $values['digital_wallet_client_generic_background'];
      $walletlayoutdata['data']['foregroundColor'] = $values['digital_wallet_client_generic_foreground'];
      $walletlayoutdata['data']['labelColor'] = $values['digital_wallet_client_generic_labelcolor'];
      $walletlayoutdata['files']['icon'] = $values['digital_wallet_client_generic_icon'];
      $walletlayoutdata['files']['icon2x'] = $values['digital_wallet_client_generic_icon_2x'];
      $walletlayoutdata['files']['logo'] = $values['digital_wallet_client_generic_logo'];

      if (!empty($values['digital_wallet_client_primaryfields_value'])) {
        $walletlayoutdata['data']['generic']['primaryFields']['0']['key'] = 'primary';
        $walletlayoutdata['data']['generic']['primaryFields']['0']['label'] = $values['digital_wallet_client_primaryfields_label'];
        $walletlayoutdata['data']['generic']['primaryFields']['0']['value'] = $values['digital_wallet_client_primaryfields_value'];
      }
      else {
        unset($walletlayoutdata['data']['generic']['primaryFields']);
      }

      if (!empty($values['digital_wallet_client_secfields1_value'])) {
        $walletlayoutdata['data']['generic']['secondaryFields']['0']['key'] = 'secondary1';
        $walletlayoutdata['data']['generic']['secondaryFields']['0']['label'] = $values['digital_wallet_client_secfields1_label'];
        $walletlayoutdata['data']['generic']['secondaryFields']['0']['value'] = $values['digital_wallet_client_secfields1_value'];
        $walletlayoutdata['data']['generic']['secondaryFields']['0']['textAlignment'] = $values['digital_wallet_client_secfields1_align'];
      }
      else {
        unset($walletlayoutdata['data']['generic']['secondaryFields']['0']);
      }

      if (!empty($values['digital_wallet_client_secfields2_value'])) {
        $walletlayoutdata['data']['generic']['secondaryFields']['1']['key'] = 'secondary2';
        $walletlayoutdata['data']['generic']['secondaryFields']['1']['label'] = $values['digital_wallet_client_secfields2_label'];
        $walletlayoutdata['data']['generic']['secondaryFields']['1']['value'] = $values['digital_wallet_client_secfields2_value'];
        $walletlayoutdata['data']['generic']['secondaryFields']['1']['textAlignment'] = $values['digital_wallet_client_secfields2_align'];
      }
      else {
        unset($walletlayoutdata['data']['generic']['secondaryFields']['1']);
      }

      if (!empty($values['digital_wallet_client_auxfields1_value'])) {
        $walletlayoutdata['data']['generic']['auxiliaryFields']['0']['key'] = 'auxfields1';
        $walletlayoutdata['data']['generic']['auxiliaryFields']['0']['label'] = $values['digital_wallet_client_auxfields1_label'];
        $walletlayoutdata['data']['generic']['auxiliaryFields']['0']['value'] = $values['digital_wallet_client_auxfields1_value'];
        $walletlayoutdata['data']['generic']['auxiliaryFields']['0']['textAlignment'] = $values['digital_wallet_client_auxfields1_align'];
      }
      else {
        unset($walletlayoutdata['data']['generic']['auxiliaryFields']['0']);
      }

      if (!empty($values['digital_wallet_client_auxfields2_value'])) {
        $walletlayoutdata['data']['generic']['auxiliaryFields']['1']['key'] = 'auxfields2';
        $walletlayoutdata['data']['generic']['auxiliaryFields']['1']['label'] = $values['digital_wallet_client_auxfields2_label'];
        $walletlayoutdata['data']['generic']['auxiliaryFields']['1']['value'] = $values['digital_wallet_client_auxfields2_value'];
        $walletlayoutdata['data']['generic']['auxiliaryFields']['1']['textAlignment'] = $values['digital_wallet_client_auxfields2_align'];
      }
      else {
        unset($walletlayoutdata['data']['generic']['auxiliaryFields']['1']);
      }

      $walletlayoutdata['data']['generic']['backFields'][0] = [
        'key' => 'backfield1',
        'label' => $values['digital_wallet_client_backfield1_label'],
        'value' => $values['digital_wallet_client_backfield1_value'],
      ];

      if (!empty($values['digital_wallet_client_backfield2_label']) || !empty($values['digital_wallet_client_backfield2_value'])) {
        $walletlayoutdata['data']['generic']['backFields'][1] = [
          'key' => 'backfield2',
          'label' => $values['digital_wallet_client_backfield2_label'],
          'value' => $values['digital_wallet_client_backfield2_value'],
        ];
      }

      if (!empty($values['digital_wallet_client_backfield3_label']) || !empty($values['digital_wallet_client_backfield3_value'])) {
        $walletlayoutdata['data']['generic']['backFields'][2] = [
          'key' => 'backfield3',
          'label' => $values['digital_wallet_client_backfield3_label'],
          'value' => $values['digital_wallet_client_backfield3_value'],
        ];
      }

      if (!empty($values['digital_wallet_client_backfield4_label']) || !empty($values['digital_wallet_client_backfield4_value'])) {
        $walletlayoutdata['data']['generic']['backFields'][3] = [
          'key' => 'backfield4',
          'label' => $values['digital_wallet_client_backfield4_label'],
          'value' => $values['digital_wallet_client_backfield4_value'],
        ];
      }

      if (!empty($values['digital_wallet_client_backfield5_label']) || !empty($values['digital_wallet_client_backfield5_value'])) {
        $walletlayoutdata['data']['generic']['backFields'][4] = [
          'key' => 'backfield5',
          'label' => $values['digital_wallet_client_backfield5_label'],
          'value' => $values['digital_wallet_client_backfield5_value'],
        ];
      }

      if (!empty($values['digital_wallet_client_backfield6_label']) || !empty($values['digital_wallet_client_backfield6_value'])) {
        $walletlayoutdata['data']['generic']['backFields'][5] = [
          'key' => 'backfield6',
          'label' => $values['digital_wallet_client_backfield6_label'],
          'value' => $values['digital_wallet_client_backfield6_value'],
        ];
      }

      $walletlayoutdata['barcode_type'] = $values['digital_wallet_client_generic_barcode'];

      if (!empty($values['digital_wallet_client_generic_icon'])) {
        if (is_numeric($values['digital_wallet_client_generic_icon']['0']) && $values['digital_wallet_client_generic_icon']['0']) {
          $icon_file = File::load($values['digital_wallet_client_generic_icon']['0']);
          if ($icon_file->isTemporary()) {
            $icon_file->setPermanent();
            $icon_file->save();
          }
        }
      }

      if (!empty($values['digital_wallet_client_generic_icon_2x'])) {
        if (is_numeric($values['digital_wallet_client_generic_icon_2x']['0']) && $values['digital_wallet_client_generic_icon_2x']['0']) {
          $icon2x_file = File::load($values['digital_wallet_client_generic_icon_2x']['0']);
          if ($icon2x_file->isTemporary()) {
            $icon2x_file->setPermanent();
            $icon2x_file->save();
          }
        }
      }

      if (!empty($values['digital_wallet_client_generic_logo'])) {
        if (is_numeric($values['digital_wallet_client_generic_logo']['0']) && $values['digital_wallet_client_generic_logo']['0']) {
          $logo_file = File::load($values['digital_wallet_client_generic_logo']['0']);
          if ($logo_file->isTemporary()) {
            $logo_file->setPermanent();
            $logo_file->save();
          }
        }
      }

      if (isset($values['digital_wallet_client_generic_thumb']) && !empty($values['digital_wallet_client_generic_thumb'])) {
        if (is_numeric($values['digital_wallet_client_generic_thumb']['0']) && $values['digital_wallet_client_generic_thumb']['0']) {
          $thumb_file = File::load($values['digital_wallet_client_generic_thumb']['0']);
          if ($thumb_file->isTemporary()) {
            $thumb_file->setPermanent();
            $thumb_file->save();
          }
          $walletlayoutdata['files']['thumbnail'] = $values['digital_wallet_client_generic_thumb'];
        }
        else {
          unset($walletlayoutdata['files']['thumbnail']);
        }
      }
      else {
        unset($walletlayoutdata['files']['thumbnail']);
      }

      if (isset($values['digital_wallet_client_generic_thumb2x']) && !empty($values['digital_wallet_client_generic_thumb2x'])) {
        if (is_numeric($values['digital_wallet_client_generic_thumb2x']['0']) && $values['digital_wallet_client_generic_thumb2x']['0']) {
          $thumb_file2x = File::load($values['digital_wallet_client_generic_thumb2x']['0']);
          if ($thumb_file2x->isTemporary()) {
            $thumb_file2x->setPermanent();
            $thumb_file2x->save();
          }

          $walletlayoutdata['files']['thumbnail2x'] = $values['digital_wallet_client_generic_thumb2x'];
        }
        else {
          unset($walletlayoutdata['files']['thumbnail2x']);
        }
      }
      else {
        unset($walletlayoutdata['files']['thumbnail2x']);
      }

      $walletlayout->data = $encoder->encode($walletlayoutdata, 'json');
    }

    return $walletlayout;
  }

  /**
   * Callback for providing the default data for layout.
   *
   * @return array|mixed
   *   The default data structure that is required for wallet webservice request.
   */
  public function defaultData() {
    $defaults['data'] = [
      'generic' => [
        'primaryFields' => [
          [
            "key" => "primary",
            "label" => "@primary_label",
            "value" => "@primary_value",
          ],
        ],
        'secondaryFields' => [
          [
            "key" => "secondary1",
            "value" => '@number',
            "label" => "cert",
            "textAlignment" => "PKTextAlignmentLeft",
          ],
          [
            "key" => "secondary2",
            "value" => '@number',
            "label" => "GROUP",
            "textAlignment" => "PKTextAlignmentLeft",
          ],
        ],
        'auxiliaryFields' => [],
        'backFields' => [],
      ],
      'serialNumber' => '@number',
      'description' => '',
      // Provide integer value as input for below parameter.
      'formatVersion' => '',
      'barcode' => [
        "format" => "PKBarcodeFormatPDF417",
        "message" => "@number",
        "messageEncoding" => "UTF-8",
        "altText" => "@number",
      ],
      'barcodes' => [
        [
          "format" => "PKBarcodeFormatPDF417",
          "message" => "@number",
          "messageEncoding" => "UTF-8",
          "altText" => "@number",
        ],
      ],
      // RGB color format required.
      'backgroundColor' => '',
      // RGB color format required.
      'foregroundColor' => '',
      // RGB color format required.
      'labelColor' => '',
      'passTypeIdentifier' => '',
      'teamIdentifier' => '',
      'organizationName' => '',
    ];

    $defaults['files'] = [
      'icon' => '',
      'icon2x' => '',
      'logo' => '',
    ];

    return $defaults;
  }

  /**
   * Callback for providing the raw data for this layout.
   *
   * @param string $layout_id
   *   The unique identifier for layout.
   *
   * @return array
   *   The raw data of the layout.
   */
  public function rawData($layout_id) {
    $layout_data = [];

    $layout_entity = \Drupal::entityTypeManager()->getStorage('walletlayout')->load($layout_id);
    $encoder = \Drupal::service('serializer.encoder.json');

    if ($layout_entity instanceof WalletLayout) {
      $layout_data = $encoder->decode($layout_entity->data, 'json');
    }

    return $layout_data;
  }

  /**
   * Provides the data required for webservice request.
   *
   * @param string $layout_id
   *   Layout ID.
   * @param string $serial_number
   *   Serial Number.
   * @param string $group_number
   *   Group Number.
   *
   * @return mixed
   *   Layout request data.
   */
  public function requestData($layout_id, $serial_number, $group_number) {
    $layout_data = [];

    $layout_entity = \Drupal::entityTypeManager()->getStorage('walletlayout')->load($layout_id);
    $encoder = \Drupal::service('serializer.encoder.json');

    if ($layout_entity instanceof WalletLayout) {
      if ($layout_entity->status) {
        $layout_data = $encoder->decode($layout_entity->data, 'json');

        if (is_numeric($layout_data['files']['icon']['0']) && $layout_data['files']['icon']['0']) {
          $icon_file = file_load($layout_data['files']['icon']['0']);
          $icon_url = file_create_url($icon_file->getFileUri());
          $layout_data['files']['icon'] = str_replace('https://', 'http://', $icon_url);
        }
        else {
          $layout_data['files']['icon'] = str_replace('https://', 'http://', $layout_data['files']['icon']);
        }

        if (is_numeric($layout_data['files']['icon2x']['0']) && $layout_data['files']['icon2x']['0']) {
          $icon2x_file = file_load($layout_data['files']['icon2x']['0']);
          $icon2x_file_url = file_create_url($icon2x_file->getFileUri());
          $layout_data['files']['icon2x'] = str_replace('https://', 'http://', $icon2x_file_url);
        }
        else {
          $layout_data['files']['icon2x'] = str_replace('https://', 'http://', $layout_data['files']['icon2x']);
        }

        if (is_numeric($layout_data['files']['logo']['0']) && $layout_data['files']['logo']['0']) {
          $logo_file = file_load($layout_data['files']['logo']['0']);
          $logo_file_url = file_create_url($logo_file->getFileUri());
          $layout_data['files']['logo'] = str_replace('https://', 'http://', $logo_file_url);
        }
        else {
          $layout_data['files']['logo'] = str_replace('https://', 'http://', $layout_data['files']['logo']);
        }

        if (isset($layout_data['files']['thumbnail']) && !empty($layout_data['files']['thumbnail'])) {
          if (is_numeric($layout_data['files']['thumbnail']['0']) && $layout_data['files']['thumbnail']['0']) {
            $thumb_file = file_load($layout_data['files']['thumbnail']['0']);
            $thumb_file_url = file_create_url($thumb_file->getFileUri());
            $layout_data['files']['thumbnail'] = str_replace('https://', 'http://', $thumb_file_url);
          }
          else {
            $layout_data['files']['thumbnail'] = str_replace('https://', 'http://', $layout_data['files']['thumbnail']);
          }
        }

        if (isset($layout_data['files']['thumbnail2x']) && !empty($layout_data['files']['thumbnail2x'])) {
          if (is_numeric($layout_data['files']['thumbnail2x']['0']) && $layout_data['files']['thumbnail2x']['0']) {
            $thumb2x_file = file_load($layout_data['files']['thumbnail2x']['0']);
            $thumb2x_file_url = file_create_url($thumb2x_file->getFileUri());
            $layout_data['files']['thumbnail2x'] = str_replace('https://', 'http://', $thumb2x_file_url);
          }
          else {
            $layout_data['files']['thumbnail2x'] = str_replace('https://', 'http://', $layout_data['files']['thumbnail2x']);
          }
        }

        if (!empty($layout_data['data']['generic']['secondaryFields']['0']) && $layout_data['data']['generic']['secondaryFields']['0']['value'] != '@cardnumber') {
          $serial_number = $layout_data['data']['generic']['secondaryFields']['0']['value'];
        }

        if (!empty($serial_number)) {
          $layout_data['data']['generic']['secondaryFields']['0']['value'] = "$serial_number";

          $layout_data['data']['serialNumber'] = "$serial_number";
          $layout_data['data']['barcode']['message'] = "$serial_number";
          $layout_data['data']['barcode']['altText'] = "$serial_number";
          $layout_data['data']['barcode']['format'] = $layout_data['barcode_type'];
          $layout_data['data']['barcodes']['0']['message'] = "$serial_number";
          $layout_data['data']['barcodes']['0']['altText'] = "$serial_number";
          $layout_data['data']['barcodes']['0']['format'] = $layout_data['barcode_type'];
        }
        else {
          unset($layout_data['data']['coupon']['secondaryFields']['0']);
          unset($layout_data['data']['serialNumber']);
          unset($layout_data['data']['barcode']);
          unset($layout_data['data']['barcodes']);
        }

        if (!empty($layout_data['data']['generic']['secondaryFields']['1']) && $layout_data['data']['generic']['secondaryFields']['1']['value'] != '@groupnumber') {
          $group_number = $layout_data['data']['generic']['secondaryFields']['1']['value'];
        }

        if ($group_number == NULL) {
          unset($layout_data['data']['generic']['secondaryFields']['1']);
        }
        else {
          $layout_data['data']['generic']['secondaryFields']['1']['value'] = "$group_number";
        }

        $layout_data['data']['backgroundColor'] = '#' . $layout_data['data']['backgroundColor'];
        $layout_data['data']['foregroundColor'] = '#' . $layout_data['data']['foregroundColor'];
        $layout_data['data']['labelColor'] = '#' . $layout_data['data']['labelColor'];

        if (empty($layout_data['data']['generic']['auxiliaryFields'])) {
          unset($layout_data['data']['generic']['auxiliaryFields']);
        }

        unset($layout_data['barcode_type']);
        $layout_data['status'] = StatusCodes::HTTP_OK;
      }
      else {
        $layout_data = [
          'data' => [],
          'status' => StatusCodes::HTTP_FORBIDDEN,
        ];
      }
    }
    else {
      $layout_data = [
        'data' => [],
        'status' => StatusCodes::HTTP_INTERNAL_SERVER_ERROR,
      ];
    }

    return $layout_data;
  }

}
