<?php

namespace Drupal\digital_wallet_client\Plugin\WalletLayoutProvider;

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\digital_wallet_client\Entity\WalletLayout;
use Drupal\digital_wallet_client\StatusCodes;
use Drupal\digital_wallet_client\WalletLayoutInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Defines a android loyalty layout interface.
 *
 * @WalletLayout(
 *   id = "loyalty",
 *   title = @Translation("Loyalty"),
 *   admintitle = @Translation("Loyalty for Android"),
 *   platform = @Translation("Android"),
 *   description = @Translation("This is a default coupon type supported by Android Library."),
 * )
 */
class AndroidLoyalty implements WalletLayoutInterface {

  /**
   * Config form for layout.
   *
   * @param \Drupal\digital_wallet_client\Entity\WalletLayout $walletlayout
   *   Wallet Layout Entity.
   *
   * @return array
   *   Layout Config form.
   */
  public function configForm(WalletLayout $walletlayout) {
    $encoder = \Drupal::getContainer()->get('serializer.encoder.json');
    $layout_data = $encoder->decode($walletlayout->data, 'json');

    $classid_options = digital_wallet_client_get_android_classes();

    if (empty($classid_options)) {
      drupal_set_message(t('Please add Android Classes before configuring Android Layouts.'), 'error', TRUE);
      $response = new RedirectResponse('/admin/config/digital-wallet/android-classes');
      $response->send();
    }

    $form['digital_wallet_client_loyalty_class_id'] = [
      '#type' => 'select',
      '#title' => t('Class ID'),
      '#default_value' => isset($layout_data['data']['classid']) ? $layout_data['data']['classid'] : '',
      '#description' => t('Class ID associated with this layout.'),
      '#options' => $classid_options,
      '#required' => TRUE,
    ];

    $form['digital_wallet_client_loyalty_account_id'] = [
      '#type' => 'textfield',
      '#title' => t('Account ID'),
      '#default_value' => isset($layout_data['data']['account_id']) ? $layout_data['data']['account_id'] : '@account_id',
      '#description' => t('This field can be used to display Card Number in App. Token for this field is @value. Keep value of this field as token to pass dynamic value to this field. Note, data of this field will be passed to Barcode field.', ['@value' => '@cardnumber']),
    ];

    $form['digital_wallet_client_loyalty_account_name'] = [
      '#type' => 'textfield',
      '#title' => t('Account Name'),
      '#default_value' => isset($layout_data['data']['account_name']) ? $layout_data['data']['account_name'] : '@account_name',
      '#description' => t('This field can be used to display Group Number in App. Token for this field is @value. Keep value of this field as token to pass dynamic value to this field.', ['@value' => '@groupnumber']),
    ];

    $form['digital_wallet_client_loyalty_barcode'] = [
      '#type' => 'details',
      '#title' => t('Barcode'),
      '#weight' => 3,
    ];

    $form['digital_wallet_client_loyalty_barcode']['digital_wallet_client_loyalty_barcodetype'] = [
      '#type' => 'select',
      '#title' => t('Type'),
      '#default_value' => isset($layout_data['data']['barcode']['type']) ? $layout_data['data']['barcode']['type'] : '',
      '#options' => $this->barcodeTypes(),
    ];

    $form['digital_wallet_client_loyalty_barcode']['digital_wallet_client_loyalty_barcodevalue'] = [
      '#type' => 'textfield',
      '#title' => t('Value'),
      '#default_value' => isset($layout_data['data']['barcode']['value']) ? $layout_data['data']['barcode']['value'] : '@cardnumber',
    ];

    $form['digital_wallet_client_loyalty_msgmodules'] = [
      '#type' => 'details',
      '#title' => t('Message Modules'),
      '#weight' => 5,
    ];

    $form['digital_wallet_client_loyalty_msgmodules']['digital_wallet_client_loyalty_msgmodule1'] = [
      '#type' => 'details',
      '#title' => t('Message Module 1'),
      '#weight' => 1,
    ];

    $form['digital_wallet_client_loyalty_msgmodules']['digital_wallet_client_loyalty_msgmodule1']['digital_wallet_client_loyalty_msgheader1'] = [
      '#type' => 'textfield',
      '#title' => t('Header'),
      '#default_value' => isset($layout_data['data']['messages']['0']['header']) ? $layout_data['data']['messages']['0']['header'] : '',
      '#description' => t('This data will be displayed as title of the card in wallet.'),
    ];

    $form['digital_wallet_client_loyalty_msgmodules']['digital_wallet_client_loyalty_msgmodule1']['digital_wallet_client_loyalty_msgbody1'] = [
      '#type' => 'textarea',
      '#title' => t('Body'),
      '#default_value' => isset($layout_data['data']['messages']['0']['body']) ? $layout_data['data']['messages']['0']['body'] : '',
    ];

    $form['digital_wallet_client_loyalty_msgmodules']['digital_wallet_client_loyalty_msgmodule2'] = [
      '#type' => 'details',
      '#title' => t('Message Module 2'),
      '#weight' => 2,
    ];

    $form['digital_wallet_client_loyalty_msgmodules']['digital_wallet_client_loyalty_msgmodule2']['digital_wallet_client_loyalty_msgheader2'] = [
      '#type' => 'textfield',
      '#title' => t('Header'),
      '#default_value' => isset($layout_data['data']['messages']['1']['header']) ? $layout_data['data']['messages']['1']['header'] : '',
      '#description' => t('This data will be displayed as title of the card in wallet.'),
    ];

    $form['digital_wallet_client_loyalty_msgmodules']['digital_wallet_client_loyalty_msgmodule2']['digital_wallet_client_loyalty_msgbody2'] = [
      '#type' => 'textarea',
      '#title' => t('Body'),
      '#default_value' => isset($layout_data['data']['messages']['1']['body']) ? $layout_data['data']['messages']['1']['body'] : '',
    ];

    $form['digital_wallet_client_loyalty_textmodules'] = [
      '#type' => 'details',
      '#title' => t('Text Modules'),
      '#weight' => 6,
    ];

    $form['digital_wallet_client_loyalty_textmodules']['digital_wallet_client_loyalty_textheader'] = [
      '#type' => 'textfield',
      '#title' => t('Header'),
      '#default_value' => isset($layout_data['data']['textModulesData']['header']) ? $layout_data['data']['textModulesData']['header'] : '',
    ];

    $form['digital_wallet_client_loyalty_textmodules']['digital_wallet_client_loyalty_textbody'] = [
      '#type' => 'textarea',
      '#title' => t('Body'),
      '#default_value' => isset($layout_data['data']['textModulesData']['body']) ? $layout_data['data']['textModulesData']['body'] : '',
    ];

    $form['digital_wallet_client_loyalty_object_links'] = [
      '#type' => 'details',
      '#title' => t('Links'),
      '#weight' => 8,
    ];

    $form['digital_wallet_client_loyalty_object_links']['digital_wallet_client_loyalty_object_links_link1'] = [
      '#type' => 'details',
      '#title' => t('Link 1'),
      '#weight' => 1,
    ];

    $form['digital_wallet_client_loyalty_object_links']['digital_wallet_client_loyalty_object_links_link1']['digital_wallet_client_loyalty_object_links_desc1'] = [
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#default_value' => isset($layout_data['data']['links']['0']['description']) ? $layout_data['data']['links']['0']['description'] : '',
      '#required' => TRUE,
    ];

    $form['digital_wallet_client_loyalty_object_links']['digital_wallet_client_loyalty_object_links_link1']['digital_wallet_client_loyalty_object_link1'] = [
      '#type' => 'textfield',
      '#title' => t('Link'),
      '#default_value' => isset($layout_data['data']['links']['0']['uri']) ? $layout_data['data']['links']['0']['uri'] : '',
      '#required' => TRUE,
    ];

    $form['digital_wallet_client_loyalty_object_links']['digital_wallet_client_loyalty_object_links_link2'] = [
      '#type' => 'details',
      '#title' => t('Link 2'),
      '#weight' => 2,
    ];

    $form['digital_wallet_client_loyalty_object_links']['digital_wallet_client_loyalty_object_links_link2']['digital_wallet_client_loyalty_object_links_desc2'] = [
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#default_value' => isset($layout_data['data']['links']['1']['description']) ? $layout_data['data']['links']['1']['description'] : '',
    ];

    $form['digital_wallet_client_loyalty_object_links']['digital_wallet_client_loyalty_object_links_link2']['digital_wallet_client_loyalty_object_link2'] = [
      '#type' => 'textfield',
      '#title' => t('Link'),
      '#default_value' => isset($layout_data['data']['links']['1']['uri']) ? $layout_data['data']['links']['1']['uri'] : '',
    ];

    $form['digital_wallet_client_loyalty_object_links']['digital_wallet_client_loyalty_object_links_link3'] = [
      '#type' => 'details',
      '#title' => t('Link 3'),
      '#weight' => 3,
    ];

    $form['digital_wallet_client_loyalty_object_links']['digital_wallet_client_loyalty_object_links_link3']['digital_wallet_client_loyalty_object_links_desc3'] = [
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#default_value' => isset($layout_data['data']['links']['2']['description']) ? $layout_data['data']['links']['2']['description'] : '',
    ];

    $form['digital_wallet_client_loyalty_object_links']['digital_wallet_client_loyalty_object_links_link3']['digital_wallet_client_loyalty_object_link3'] = [
      '#type' => 'textfield',
      '#title' => t('Link'),
      '#default_value' => isset($layout_data['data']['links']['2']['uri']) ? $layout_data['data']['links']['2']['uri'] : '',
    ];

    return $form;
  }

  /**
   * Config form submit.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   * @param \Drupal\digital_wallet_client\Entity\WalletLayout $walletlayout
   *   Wallet Layout Entity.
   * @param bool $new
   *   Indicates if entity is new.
   */
  public function configFormSubmit(array $form, FormStateInterface &$form_state, WalletLayout $walletlayout, $new = FALSE) {
    $encoder = \Drupal::service('serializer.encoder.json');
    $walletlayout->set('platform', 'Android');

    if ($new) {
      $default_data = $this->defaultData();
      $walletlayout->set('created', time());
      $walletlayout->data = $encoder->encode($default_data, 'json');
    }
    else {
      $values = $form_state->getValues();
      $walletlayoutdata = $encoder->decode($walletlayout->data, 'json');

      $walletlayoutdata['data']['classid'] = $values['digital_wallet_client_loyalty_class_id'];
      $walletlayoutdata['data']['account_id'] = $values['digital_wallet_client_loyalty_account_id'];
      $walletlayoutdata['data']['account_name'] = $values['digital_wallet_client_loyalty_account_name'];
      $walletlayoutdata['data']['barcode']['type'] = $values['digital_wallet_client_loyalty_barcodetype'];
      $walletlayoutdata['data']['barcode']['value'] = $values['digital_wallet_client_loyalty_barcodevalue'];
      $walletlayoutdata['data']['messages']['0']['header'] = $values['digital_wallet_client_loyalty_msgheader1'];
      $walletlayoutdata['data']['messages']['0']['body'] = $values['digital_wallet_client_loyalty_msgbody1'];

      if (!empty($values['digital_wallet_client_loyalty_msgheader2']) && !empty($values['digital_wallet_client_loyalty_msgbody1'])) {
        $walletlayoutdata['data']['messages']['1']['header'] = $values['digital_wallet_client_loyalty_msgheader2'];
        $walletlayoutdata['data']['messages']['1']['body'] = $values['digital_wallet_client_loyalty_msgbody2'];
      }
      else {
        unset($walletlayoutdata['data']['messages']['1']);
      }

      $walletlayoutdata['data']['textModulesData']['header'] = $values['digital_wallet_client_loyalty_textheader'];
      $walletlayoutdata['data']['textModulesData']['body'] = $values['digital_wallet_client_loyalty_textbody'];
      $walletlayoutdata['data']['links']['0']['description'] = $values['digital_wallet_client_loyalty_object_links_desc1'];
      $walletlayoutdata['data']['links']['0']['uri'] = $values['digital_wallet_client_loyalty_object_link1'];

      if (!empty($values['digital_wallet_client_loyalty_object_links_desc2']) && !empty($values['digital_wallet_client_loyalty_object_link2'])) {
        $walletlayoutdata['data']['links']['1']['description'] = $values['digital_wallet_client_loyalty_object_links_desc2'];
        $walletlayoutdata['data']['links']['1']['uri'] = $values['digital_wallet_client_loyalty_object_link2'];
      }
      else {
        unset($walletlayoutdata['data']['links']['1']);
      }

      if (!empty($values['digital_wallet_client_loyalty_object_links_desc3']) && !empty($values['digital_wallet_client_loyalty_object_link3'])) {
        $walletlayoutdata['data']['links']['2']['description'] = $values['digital_wallet_client_loyalty_object_links_desc3'];
        $walletlayoutdata['data']['links']['2']['uri'] = $values['digital_wallet_client_loyalty_object_link3'];
      }
      else {
        unset($walletlayoutdata['data']['links']['2']);
      }

      $walletlayout->data = $encoder->encode($walletlayoutdata, 'json');
    }

    return $walletlayout;
  }

  /**
   * Callback for providing the default data for layout.
   *
   * @return array|mixed
   *   The default data structure that is required for wallet webservice request.
   */
  public function defaultData() {
    $defaults['data'] = [
      'classid' => '',
      'objecttype' => 'LoyaltyObject',
      'account_id' => '@cardnumber',
      'account_name' => '@groupnumber',
      'barcode' => [
        'type' => 'pdf417',
        'value' => '@cardnumber',
      ],
      'messages' => [
        [
          'header' => '',
          'body' => '',
        ],
      ],
      'textModulesData' => [
        'header' => '',
        'body' => '',
      ],
      'links' => [
        [
          'description' => '',
          'uri' => '',
        ],
      ],
    ];

    return $defaults;
  }

  /**
   * Callback for providing the raw data for this layout.
   *
   * @param string $layout_id
   *   The unique identifier for layout.
   *
   * @return array
   *   The raw data of the layout.
   */
  public function rawData($layout_id) {
    $layout_data = [];

    $layout_entity = \Drupal::entityTypeManager()->getStorage('walletlayout')->load($layout_id);
    $encoder = \Drupal::service('serializer.encoder.json');

    if ($layout_entity instanceof WalletLayout) {
      $layout_data = $encoder->decode($layout_entity->data, 'json');
    }

    return $layout_data;
  }

  /**
   * Provides the data required for webservice request.
   *
   * @param string $layout_id
   *   Layout ID.
   * @param string $serial_number
   *   Serial Number.
   * @param string $group_number
   *   Group Number.
   *
   * @return mixed
   *   Layout request data.
   */
  public function requestData($layout_id, $serial_number, $group_number) {
    $layout_data = [];
    $config = \Drupal::config('digital_wallet_client.main_settings');
    $origin = $config->get('site_base_url');

    $layout_entity = \Drupal::entityTypeManager()->getStorage('walletlayout')->load($layout_id);
    $encoder = \Drupal::service('serializer.encoder.json');

    if ($layout_entity instanceof WalletLayout) {
      if ($layout_entity->status) {
        $layout_data = $encoder->decode($layout_entity->data, 'json');

        $layout_data['data']['objectid'] = 'LoyaltyObject' . time();

        if ($layout_data['data']['account_id'] == '@account_id') {
          $layout_data['data']['account_id'] = $serial_number;
        }

        if ($layout_data['data']['account_name'] == '@account_name') {
          $layout_data['data']['account_name'] = $group_number;
        }

        if ($layout_data['data']['barcode']['value'] == 'barcode_value') {
          $layout_data['data']['barcode']['value'] = $serial_number;
        }

        $layout_data['data']['origin'] = !empty($origin) ? $origin : '';

        $layout_data['status'] = StatusCodes::HTTP_OK;
      }
      else {
        $layout_data = [
          'data' => [],
          'status' => StatusCodes::HTTP_FORBIDDEN,
        ];
      }
    }
    else {
      $layout_data = [
        'data' => [],
        'status' => StatusCodes::HTTP_INTERNAL_SERVER_ERROR,
      ];
    }

    return $layout_data;
  }

  /**
   * Callback function to return the list of available barcode types for android.
   *
   * @return array
   *   Android Barcode types.
   */
  public function barcodeTypes() {
    return [
      'pdf417' => 'PDF 417',
      'qrCode' => 'QR Code',
      'aztec' => 'Aztec',
      'codabar' => 'Codabar',
      'code128' => 'Code 128',
      'code39' => 'Code 39',
      'dataMatrix' => 'Data Matrix',
      'ean13' => 'EAN 13',
      'ean8' => 'EAN 8',
      'itf14' => 'ITF 14',
      'textOnly' => 'Text Only',
      'upcA' => 'UPC A',
      'upcE' => 'UPC E',
    ];
  }

}
