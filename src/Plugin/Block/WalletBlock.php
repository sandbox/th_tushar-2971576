<?php

namespace Drupal\digital_wallet_client\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Class WalletBlock.
 *
 * @package Drupal\digital_wallet_client\Plugin\Block
 *
 * @Block(
 *   id = "wallet_block",
 *   admin_label = @Translation("Wallet Block"),
 *   category = @Translation("Wallet"),
 * )
 */
class WalletBlock extends BlockBase {

  /**
   * Block Build function.
   *
   * {@inheritdoc}
   */
  public function build() {
    $wallet_links = digital_wallet_client_block_display('', '', '', '');

    return $wallet_links;
  }

}
