(function ($, Drupal) {

  Drupal.behaviors.digital_wallet_client = {
    attach: function (context, settings) {
      // Javascript to Show/Hide the Apple Wallet QR Code image.
      $('.apple-link-desktop img', context).once().click(function () {
        $('.apple-link-qrcode', context).slideToggle();
      });
    }
  };

}(jQuery, Drupal));
